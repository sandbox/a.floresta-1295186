<?php
/**
 * Returns the results of the commerce_payment_transaction_load() for the specified payment.
 *
 * @param $transaction_id
 *   The payment transaction ID we want to return.
 * @return
 *   payment Transaction object or FALSE if not found.
 *
 * @see commerce_payment_transaction_load() 
 */
function _payment_resource_retrieve($transaction_id) {
    $transaction = commerce_payment_transaction_load($transaction_id);
    
    // TODO: Gotta be tested and there are more work to be done. What work? I don't know yet.
    
    return $transaction;
}


/**
 * Determine whether the current user can access a payment resource.
 *
 * @param $op
 *   One of view, update, create, delete per node_access().
 * @param $args
 *   Resource arguments passed through from the original request.
 * @return bool
 *
 * @see TODO
 */
function _payment_resource_access($op = 'view', $args = array()) {

    // TODO: Everything.
    return TRUE;
}


/**
 * Creates a new payment profile based on submitted values.
 *
 * @param $payment
 *   Array representing the attributes a payment profile edit form would submit.
 * @return
 *   TODO: Don't know yet.
 *
 * @see TODO:
 * 
 */
function _payment_resource_create($transaction) {
    
}

/**
 * Determine whether the current user can create a payment resource.
 *
 * @param $op
 *   One of view, update, create, delete per node_access().
 * @param $args
 *   Resource arguments passed through from the original request.
 * @return bool
 *
 * @see TODO
 */
function _payment_resource_create($op = 'view', $args = array()) {

    // TODO: Everything.
    return TRUE;
}