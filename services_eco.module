<?php

/**
 * Implements hook_permission().
 */
function services_eco_permission() {
  return array(
    "retrieve customers data" => array(
      'title' => t("Retrieve Customer's data"),
    ),
    // TODO: design all the needed permission sets
    
  );
}

/**
 * Implements hook_services_resources().
 */
function services_eco_services_resources() {
  return array(
    'customer' => array(
      'retrieve' => array(
        'help' => 'Retrieves customer profile data from Drupal Commerce',
        'file' => array('type' => 'inc', 'module' => 'services_eco', 'name' => 'resources/customer_resource'
        ),
        'callback' => '_customer_resource_retrieve',
        'access callback' => '_customer_resource_access',
        'access arguments' => array('view'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'profile_id',
            'type' => 'int',
            'description' => 'The customer profile Id',
            'source' => array('path' => '0'),
            'optional' => FALSE,
          ),
        ),
      ),
      'create' => array(
        'help' => 'Creates a new customer profile for Drupal Commerce',
        'file' => array('type' => 'inc', 'module' => 'services_eco', 'name' => 'resources/customer_resource'),
        'callback' => '_customer_resource_create',
        'args' => array(
          array(
            'name' => 'profile',
            'optional' => FALSE,
            'source' => 'data',
            'description' => 'The customer profile data to be created',
            'type' => 'array',
          ),
        ),
        'access callback' => '_customer_resource_create',
        'access arguments' => array('create'),
        'access arguments append' => TRUE,
      ),
      'update' => array(
        'help' => 'Updates a customer profile at Drupal Commerce',
        'file' => array('type' => 'inc', 'module' => 'services_eco', 'name' => 'resources/customer_resource'),
        'callback' => '_customer_resource_update',
        'args' => array(
          array(
            'name' => 'profile_id',
            'type' => 'int',
            'description' => 'The customer profile Id to get',
            'source' => array('path' => '0'),
            'optional' => FALSE,
          ),
          array(
            'name' => 'profile',
            'optional' => FALSE,
            'source' => 'data',
            'description' => 'The customer profile data to be updated',
            'type' => 'array',
          ),
        ),
        'access callback' => '_customer_resource_access',
        'access arguments' => array('update'),
        'access arguments append' => TRUE,
      ),
    ),
    'order' => array(
      'retrieve' => array(
        'help' => 'Retrieves an order from Drupal Commerce',
        'file' => array('type' => 'inc', 'module' => 'services_eco', 'name' => 'resources/order_resource'
        ),
        'callback' => '_order_resource_retrieve',
        'access callback' => '_order_resource_access',
        'access arguments' => array('view'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'order_id',
            'type' => 'int',
            'description' => 'The order Id',
            'source' => array('path' => '0'),
            'optional' => FALSE,
          ),
        ),
      ),
      'create' => array(
        'help' => 'Creates a new order for Drupal Commerce',
        'file' => array('type' => 'inc', 'module' => 'services_eco', 'name' => 'resources/order_resource'),
        'callback' => '_order_resource_create',
        'args' => array(
          array(
            'name' => 'order',
            'optional' => FALSE,
            'source' => 'data',
            'description' => 'The order data to be created',
            'type' => 'array',
          ),
        ),
        'access callback' => '_order_resource_create',
        'access arguments' => array('create'),
        'access arguments append' => TRUE,
      ),
      'update' => array(
        'help' => 'Updates an order at Drupal Commerce',
        'file' => array('type' => 'inc', 'module' => 'services_eco', 'name' => 'resources/order_resource'),
        'callback' => '_order_resource_update',
        'args' => array(
          array(
            'name' => 'order_id',
            'type' => 'int',
            'description' => 'The order Id to get',
            'source' => array('path' => '0'),
            'optional' => FALSE,
          ),
          array(
            'name' => 'order',
            'optional' => FALSE,
            'source' => 'data',
            'description' => 'The order data to be updated',
            'type' => 'array',
          ),
        ),
        'access callback' => '_order_resource_access',
        'access arguments' => array('update'),
        'access arguments append' => TRUE,
      ),
    ),
    'payment' => array(
      'retrieve' => array(
        'help' => 'Retrieves a payment transaction from Drupal Commerce',
        'file' => array('type' => 'inc', 'module' => 'services_eco', 'name' => 'resources/payment_resource'
        ),
        'callback' => '_payment_resource_retrieve',
        'access callback' => '_payment_resource_access',
        'access arguments' => array('view'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'transaction_id',
            'type' => 'int',
            'description' => 'The Payment Transaction Id',
            'source' => array('path' => '0'),
            'optional' => FALSE,
          ),
        ),
      ),
      'create' => array(
        'help' => 'Creates a new Payment Transaction for an order in Drupal Commerce',
        'file' => array('type' => 'inc', 'module' => 'services_eco', 'name' => 'resources/payment_resource'),
        'callback' => '_payment_resource_create',
        'args' => array(
          array(
            'name' => 'transaction',
            'optional' => FALSE,
            'source' => 'data',
            'description' => 'The Payment Transaction data to be created',
            'type' => 'array',
          ),
        ),
        'access callback' => '_payment_resource_create',
        'access arguments' => array('create'),
        'access arguments append' => TRUE,
      ),
      'update' => array(
        'help' => 'Updates a Payment Transaction for an Order in Drupal Commerce',
        'file' => array('type' => 'inc', 'module' => 'services_eco', 'name' => 'resources/payment_resource'),
        'callback' => '_payment_resource_update',
        'args' => array(
          array(
            'name' => 'transaction_id',
            'type' => 'int',
            'description' => 'The Payment Transaction Id to get',
            'source' => array('path' => '0'),
            'optional' => FALSE,
          ),
          array(
            'name' => 'transaction',
            'optional' => FALSE,
            'source' => 'data',
            'description' => 'The Payment Transaction data to be updated',
            'type' => 'array',
          ),
        ),
        'access callback' => '_payment_resource_access',
        'access arguments' => array('update'),
        'access arguments append' => TRUE,
      ),
    ),
    'product' => array(
      'retrieve' => array(
        'help' => 'Retrieves a product from Drupal Commerce',
        'file' => array('type' => 'inc', 'module' => 'services_eco', 'name' => 'resources/product_resource'
        ),
        'callback' => '_product_resource_retrieve',
        'access callback' => '_product_resource_access',
        'access arguments' => array('view'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'product_id',
            'type' => 'int',
            'description' => 'The Product Id',
            'source' => array('path' => '0'),
            'optional' => FALSE,
          ),
        ),
      ),
      'create' => array(
        'help' => 'Creates a new product for Drupal Commerce',
        'file' => array('type' => 'inc', 'module' => 'services_eco', 'name' => 'resources/product_resource'),
        'callback' => '_product_resource_create',
        'args' => array(
          array(
            'name' => 'product',
            'optional' => FALSE,
            'source' => 'data',
            'description' => 'The Product data to be created',
            'type' => 'array',
          ),
        ),
        'access callback' => '_product_resource_create',
        'access arguments' => array('create'),
        'access arguments append' => TRUE,
      ),
      'update' => array(
        'help' => 'Updates a Product in Drupal Commerce',
        'file' => array('type' => 'inc', 'module' => 'services_eco', 'name' => 'resources/product_resource'),
        'callback' => '_product_resource_update',
        'args' => array(
          array(
            'name' => 'product_id',
            'type' => 'int',
            'description' => 'The Product Id to get',
            'source' => array('path' => '0'),
            'optional' => FALSE,
          ),
          array(
            'name' => 'product',
            'optional' => FALSE,
            'source' => 'data',
            'description' => 'The product data to be updated',
            'type' => 'array',
          ),
        ),
        'access callback' => '_product_resource_access',
        'access arguments' => array('update'),
        'access arguments append' => TRUE,
      ),
    ),
  );
}
